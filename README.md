# Encrypted Machine Learning

***This repository contains my exploration with different Homomorphic Encryption Techniques for Machine Learning and Federated Learning.***

# Requirements

**1. Python 3.5 +**

**2. python-paillier**

To use this, clone the repository using:

```
git clone https://github.com/n1analytics/python-paillier.git
```

Go inside the "python-paillier" directory using command prompt and run:

```
python3 setup.py install
```

**3. Numpy [+mkl for Windows]**

**4. Scikit-Learn**

**5. Jupyter Notebook**
